/**
 * Created by Mikhail Gusev on 28.10.16.
 */

var express = require('express');
var bodyParser = require('body-parser');
var libs = process.cwd() + '/libs';

var api = require(libs + '/routes/api');
var booster_results = require(libs + '/routes/booster_results');
var index = require(libs + '/routes/index');


var config = require('./config');

var app = express();

var apiVersion = config.get('api-version');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(libs + 'public'));


app.use('/', api);
app.use('/api', api);
app.use('/api/booster_results', booster_results);
app.use('/index', index);

module.exports = app;


