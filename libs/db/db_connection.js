/**
 * Created by Mikhail Gusev on 26.10.16.
 */

var mongoose = require('mongoose');

var libs = process.cwd() + '/libs/';
var log = require(libs + 'log')(module);
var config = require(libs + 'config');

mongoose.connect(config.get('mongoose:uri'));

var db = mongoose.connection;
db.once('open', function() {
    log.info('Alright! We are connected!');
});

module.exports = mongoose;
