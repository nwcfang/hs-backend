/**
 * Created by Mikhail Gusev on 26.10.16.
 */

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var AccessToken = new Schema({
    userId: {
        type: String,
        required: true
    },

    clientId: {
        type: String,
        required: true
    },

    token: {
        type: String,
        required: true,
        unique: true
    },

    created: {
        type: Date,
        default: Date.now
    }

});

module.exports = mongoose.model('AccessToken', AccessToken);

