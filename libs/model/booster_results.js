/**
 * Created by Mikhail Gusev on 26.10.16.
 */

var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var Booster = new Schema({
    common: {
        type: Number,
        required: true
    },
    rare: {
        type: Number,
        required: true
    },
    epic: {
        type: Number,
        required: true
    },
    legend: {
        type: Number,
        required: true
    }
});

module.exports = mongoose.model('Booster', Booster);
