/**
 * Created by Mikhail Gusev on 28.10.16.
 */

var mongoose = require('mongoose');

var Card = new mongoose.Schema({
    name: { type: String, required: true },
    description: { type: String },
    image_url: { type: String, required: true },
    hero: { type: String },
    type: { type: String, required: true },
    quality: { type: String, required: true },
    race: { type: String },
    setting: { type: String, required: true },
    mana: { type: Number, required: true },
    attack: { type: Number },
    health: { type: Number },
    collectible: { type: Boolean, required: true },
    effect_list: { type: Array }
});

module.exports = mongoose.model('Card', Card);