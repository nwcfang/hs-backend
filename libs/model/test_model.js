/**
 * Created by Mikhail Gusev on 31.10.16.
 */

var mongoose = require('mongoose');

var TestCard = mongoose.Schema({
    name: { type: String },
    number: { type: Number }
});

module.exports = mongoose.model('TestCard', TestCard);
