/**
 * Created by Mikhail Gusev on 26.10.16.
 */

var nconf = require('nconf');

nconf.argv()
    .env()
    .file({
        file: process.cwd() + '/config.json'
    });

module.exports = nconf;
