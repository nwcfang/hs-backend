/**
 * Created by Mikhail Gusev on 28.10.16.
 */
var express = require('express');
var router = express.Router();

var libs = process.cwd() + '/libs/';
var db = require(libs + 'db/db_connection');
var log = require(libs + 'log')(module);
var Booster = require(libs + 'model/booster_results');

router.get('/', function(req, res){
   log.info('We are here');
   Booster.find(function (err, boosters) {
      if (!err) {
         log.info('Send responce');
         return res.json(boosters)
      } else {
         res.statusCode = 500;

         log.error('Internal error');

         return res.json({
            error: 'Server error'
         })
      }
   }) ;
});
router.post('/', function(req, res){
  var booster = new Booster({
    common: req.body.common,
    rare: req.body.rare,
    epic: req.body.epic,
    legend: req.body.legend
  });
  console.log(req.body);

  booster.save(function (err) {
    if (!err) {
      log.info("Booster created");
      return res.send({ status: 'OK', booster:booster });
    } else {
      console.log(err);
      if(err.name == 'ValidationError') {
        res.statusCode = 400;
        res.send({ error: 'Validation error' });
      } else {
        res.statusCode = 500;
        res.send({ error: 'Server error' });
      }
      log.error('Internal error(%d): %s',res.statusCode,err.message);
    }
  });
});
/*router.get('/', function(req, res){
   res.json({
      msg: 'Hello, its API'
   });
});*/

/*router.get('/:id', function(req, res){

});*/

module.exports = router;
