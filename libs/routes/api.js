/**
 * Created by Mikhail Gusev on 28.10.16.
 */

var express = require('express');

var router = express.Router();

router.get('/', function(req, res){
    res.json({
        msg: 'Hello, its API'
    });
});

module.exports = router;
