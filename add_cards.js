/**
 * Created by Mikhail Gusev on 27.10.16.
 */

var cards = require('./cards/all-cards.json').cards;
var boosters = require('./test-boosters.json').boosters;
var libs = process.cwd() + '/libs/';
var log = require(libs + 'log')(module);
var db = require(libs + 'db/db_connection');
var Card = require(libs + 'model/card');
var TestCard = require(libs + 'model/test_model');
var Booster = require(libs + 'model/booster_results');

Booster.remove({}, function(err){
    log.info('We in Booster!');
    for (var booster in boosters) {
        log.info(boosters[booster]);
        var newBooster = new Booster(boosters[booster]);
        newBooster.save(function(err, newCard) {
                if(!err) {
                    log.info("New booster - %s", newBooster.id);
                } else {
                    return log.error("In has error", err);
                }})
    }
});



/*
Card.remove({}, function(err){
    for (var card in cards){
        if (cards.hasOwnProperty(card)){
            cards[card]['setting'] = cards[card]['set'];
            delete cards[card]['id'];
            delete cards[card]['set'];
            var dict = cards[card];
            if (dict.mana == null ) {dict.mana = 0}
            if (!dict.hasOwnProperty('effect_list')) {
                dict['effect_list'] = [];
                log.debug(dict.hasOwnProperty('effect_list'))
            }
            var name = dict.name;
            var newCard = new Card (dict);
            newCard.save(function(err, newCard) {
                if(!err) {
                    log.info("New card - %s:%s:%s", newCard.name, newCard.setting, newCard.id);
                }else {
                    return log.error("In %s has error", name, err);
                    //return log.error('Error');
                }
            });
        }

    }

});
 */

setTimeout(function() {
    db.disconnect();
}, 30000);



