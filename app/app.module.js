/**
 * Created by Mikhail Gusev on 18.10.16.
 */

(function(){
   'use strict';
   angular.module('hsDeck', [
      'ui.bootstrap',
      'countPanel',
      'openBooster'
   ]);
})();
