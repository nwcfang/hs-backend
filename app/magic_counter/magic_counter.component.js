/**
 * Created by Mikhail Gusev on 18.10.16.
 */

(function(){
    angular
        .module('magicCounter')
        .component('magicCounter', {
            templateUrl: 'magic_counter/magic_counter.template.html',
            controller: MagicCounterController,
            bindings: {
                minions: '<',
                spells: '<'
            }
        });

    function MagicCounterController(){

        this.updateMinions = function(increment) {
            var newValue = this.minions.count + increment;
            this.minions.setCount(newValue < 0 ? 0 : newValue);
        };

        this.updateSpells = function(increment) {
            var newValue = this.spells.count + increment;
            this.spells.setCount(newValue < 0 ? 0 : newValue);
        };

        this.changeAttr = function (attr, number) {
            this[attr] += number;
            if (this[attr] < 0) this[attr] = 0;
        }

    }

})();
