/**
 * Created by Mikhail Gusev on 07.12.16.
 */

(function () {
    'use strict';

    angular
        .module('openBooster')
        .component('openBooster', {
            templateUrl: 'open_booster/open_booster.template.html',
            controller: OpenBoosterController,
            binding: '='
        })


    OpenBoosterController.$inject = ['$http'];

    /* @ngInject */
    function OpenBoosterController($http) {
        var vm = this;
        vm.title = 'OpenBoosterController';
        vm.common = 0;
        vm.rare = 0;
        vm.epic = 0;
        vm.legend = 0;



        activate();

        ////////////////

        vm.addValue = function (value, whichOne){
            var newValue = vm[whichOne] + value;
            if (!(newValue > 5) && !(newValue < 0)) {
                vm[whichOne] = newValue;
            }
        };

        function activate() {
            $http.get("http://localhost:1337/api/booster_results").then(function(response){
                console.log(response);
            })
        }
    }


})();


