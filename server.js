/**
 * Created by Mikhail Gusev on 23.10.16.
 */

var express = require('express');
var path = require('path');
//var favicon = require('serve-favicon');
var log = require('./libs/log')(module);

app = express();
//app.use(favicon(__dirname + '/public/favicon.ico'));
//app.use(express.logger('dev')); // выводим все запросы со статусами в консоль
//app.use(express.bodyParser()); // стандартный модуль, для парсинга JSON в запросах
//app.use(express.methodOverride()); // поддержка put и delete
app.use(app.router); // модуль для простого задания обработчиков путей
//app.use(express.static(path.join(__dirname, "public")));

app.get('/api', function (req, res) {
    res.send('API is running');
});

app.listen('1337', function(){
    console.log('Express listening port 1337');

});

//var http = require('http');
//var url = require('url');
//
//
//var server = new http.Server(function(req, res){
//    console.log(req.method, req.url);
//    var urlParsed =  url.parse(req.url, true);
//    console.log(urlParsed.query.test);
//    if (urlParsed.pathname == '/hello' && urlParsed.query.test){
//        res.end(urlParsed.query.test);
//    } else {
//        debugger;
//        res.statusCode = 404;
//        res.end("Page not found!")
//    }
//
//});
//
//server.listen(1337, '127.0.0.1');
